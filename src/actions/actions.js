// @flow

const PagesEnum = Object.freeze({ 
    'about': 0,
    'projects': 1,
    'contact': 2,
});

const SHOW_PAGE = 'SHOW_PAGE'

export type Action = {
    type: string,
    payload: Object,
}

export type MainDispatcher = (action: Action) => any

export type Dispatcher = (action: Action) => any

const sendShowPageRequest= (pageShown: number) => ({
    type: SHOW_PAGE,
    payload: {
        pageShown,
    },
})

export {
    PagesEnum,
    SHOW_PAGE,
    sendShowPageRequest
}
