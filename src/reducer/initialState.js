// @flow

import { PagesEnum } from '../actions/actions'

export default {
    pageShown: PagesEnum.about,
};
