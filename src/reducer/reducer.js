// @flow

import {
    PagesEnum,
    SHOW_PAGE,
    sendShowPageRequest
} from '../actions/actions'

import type { Action } from '../actions/actions'
import type { MainReactProps } from '../Main'

const pageShownReducer = (
    state: Number,
    action: Action
) => {
    switch(action.type) {
        case SHOW_PAGE:
            return action.payload.pageShown;
        default:
            return state;
    }
}

const reducer = (
    state,
    action: Action
) => ({
    pageShown: pageShownReducer(state.pageShown, action),
})

export default reducer;
