// @flow

import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
    PagesEnum,
    SHOW_PAGE,
    sendShowPageRequest
} from '../actions/actions';
import PropTypes from 'prop-types';

import { Header } from '../Header'
import { AboutPage } from '../AboutPage'
import { ProjectsPage } from '../ProjectsPage'
import { Footer } from '../Footer'
import './Main.css';

import type { MainDispatcher } from '../actions/actions'
import type { HeaderProps } from '../Header'

export type MainProps = {
    pageShown: Number,
}

export type MainReactProps = MainProps & {
  openAbout: () => any,
  openProjects: () => any,
}

export const Main = (props: MainReactProps) => {
    var page;
    switch (props.pageShown) {
        case PagesEnum.about:
            page = <AboutPage />;
            break;
        case PagesEnum.projects:
            page = <ProjectsPage />;
            break;
        default:
            page = <AboutPage />;
            break;
    }

    const headerProps: HeaderProps = {
        openAbout: props.openAbout,
        openProjects: props.openProjects,
    }

    return(
        <div className="main">
            <Header {...headerProps} />
            {page}
            <Footer />
        </div>
    )
}


function mapStateToProps(state) {
  return {
    pageShown: state.pageShown
  };
}

export const mapDispatchToProps = (dispatch: MainDispatcher) => ({
  openAbout: () => dispatch(sendShowPageRequest(PagesEnum.about)),
  openProjects: () => dispatch(sendShowPageRequest(PagesEnum.projects)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Main);