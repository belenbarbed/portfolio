// @flow

import React from 'react';
import { render } from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import configureStore from './store/configureStore';
import { Provider } from 'react-redux'

import { Main } from './Main';
import type { MainProps } from './Main';

const store = configureStore();

const props: MainProps = {
    pageShown: 0,
}

// ReactDOM.render(<Main {...props} />, document.getElementById('root'));
// registerServiceWorker();

// import React from 'react';
// import { render } from 'react-dom';
// import configureStore from './store/configureStore';
// import SomeComponent from './components/SomeComponent/SomeComponent';
// import './styles/styles.css';

// const store = configureStore();

render(
  <Provider store={store}>
      <Main />
  </Provider>,
  document.getElementById('root')
);
