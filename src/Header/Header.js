// @flow

import React from 'react';

import './Header.css';

export type HeaderProps = {
    openAbout: () => any,
    openProjects: () => any,
}

export const Header = (props: HeaderProps) => (
    <div className="header">
        <p>Header</p>
    </div>
)
