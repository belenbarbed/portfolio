// @flow

import React from 'react';

import './AboutPage.css';

export const AboutPage = () => (
    <div className="about-page">
        <div class="image"><p></p></div>
        <div class="content-page-content">
          <p>Hi! I'm Bel&eacute;n, and I'm currently undergoing my Third year at Imperial College London studying Electronic and Information Engineering.</p>
          <p>Although the course title may not infer much, it has given me a good foothold in the worlds of both Hardware and Software, with modules such as Computer Architecture, Databases, Digital Electronics, Algorithms, and Machine Learning.</p>
          <p>Outside of the strictly academic, I enjoy spending time in our beloved Robotics Lab, where, as <a href="http://icrs.io" target="_blank">ICRS</a> (Imperial College Robotics Society) we maintain a space for enthusiasts and pros alike to develop their skills as well as sign-up for courses, enter competitions, and participate in hackathons, while also being able to use our tools for cool projects and rapid prototyping. I'm lucky to have been elected Chair of ICRS for the second year in a row, so I'm looking forward to another year of robotics, hacking, and the heaps of admin that comes with it.</p>
          <p>To top it all off, I am now in the middle of my 6-month placement at Amazon Video, where I work with the front-end team to improve the sign-up experience for Prime services.</p>
        </div>
    </div>
)
